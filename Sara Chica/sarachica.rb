require  'test/unit'

def celula_sigue_viva(vecina)
  return false
end

def celula_nace(vecina)
  return false
end

class TestCelulaMuerta < Test::Unit::TestCase
  def test_si_celula_muerta_3_celulas_vecinas_vivas_deberia_nacer
    assert_equal true, celula_nace(3)
  end

  def test_si_celula_muerta_1_celula_vecina_viva_no_deberia_nacer
    assert_equal false, celula_nace(1)
  end

  def test_si_celula_muerta_2_celulas_vecinas_vivas_no_deberia_nacer
    assert_equal false, ceula_nace(2)
  end
end

class TestCelulaViva < Test::Unit::TestCase
  def test_si_celula_viva_2_celulas_vecinas_vivas_deberia_vivir
    assert_equal true, celula_sigue_viva(2)
  end

  def test_si_celula_viva_3_celulas_vecinas_vivas_deberia_vivir
    assert_equal true, celula_sigue_viva(3)
  end

  def test_si_celula_viva_1_celula_vecina_viva_deberia_morir
    assert_equal false, celula_sigue_viva(1)
  end 

  def test_si_celula_viva_4_celulas_vecinas_vivas_deberia_morir
    assert_equal false, celula_sigue_viva(4)
  end

end


