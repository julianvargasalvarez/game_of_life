require 'rspec'

class GameOfLifeCell
  attr_accessor :dead

  def initialize(x, y, status, n)
    self.dead = true
  end
end

describe "Game of life rules" do
  describe "steps for changing the state for a given cell" do
    context "when the given cell is alive" do
      context "and it has only one neighbor" do
        it "should died for underpopulation" do
          cell = GameOfLifeCell.new(1, 2, true, 1)
          expect(cell.dead).to eq true
        end
      end
    end

    context "when the given cell is dead" do
    end
  end
end
