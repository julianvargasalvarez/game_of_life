require 'test/unit'

def celdaMoriraBajaPoblacion(celda)
end

def viviraSiguienteGeneracion(celda)
end

def celdaMoriraSobrePoblacion(celda)
end

def viviraReproduccion(celda)
end

class TestGameOfLive < Test::Unit::TestCase 
  def test_regla_1(celda)
    assert_equal true, celdaMoriraBajaPoblacion(celda)
  end

  def test_regla_2(celda)
    assert_equal false, viviraSiguienteGeneracion(celda)
  end

  def test_regla_3(celda)
    assert_equal true, celdaMoriraSobrePoblacion(celda)
  end

  def test_regla_4(celda)
    assert_equal false, viviraReproduccion(celda)
  end

end
