require 'rspec'

class GameOfLifeCell
  attr_accessor :dead

  def initialize (x,y,status,n)
    self.dead = status
  end
end

describe "Game of life rules" do
  describe "steps for changing the state for a given cell" do

    context "When the given cell is alive" do
      context "and it has only one neighbour" do
        it "should die by under-population"
          cell = GameOfLifeCell.new(1,2,true,1)
          expect(cell.dead).to eq true
        end
      end

      context "and it has just two neighbours" do
        it "should live to next generation" do
          cell = GameOfLifeCell.new(1,2,true,1)
          expect(cell.dead).to eq false
        end
      end

      context "and it has just three neighbours" do
        it "should live to next generation" do
          cell = GameOfLifeCell.new(1,2,true,1)
          expect(cell.dead)to eq false
        end
      end

      context "and it has four neighbours" do
        it "should die by overcrowding" do
          cell = GameOfLifeCell.new(1,2,true,1)
          expect(cell.dead)to eq true
        end
      end

      #the neighbours could be diagonally adjacent too
      context "and it has five neighbours" do
        it "should die by overcrowding" do
          cell = GameOfLifeCell.new(1,2,true,1)
          expect(cell.dead)to eq true
        end
      end

      context "and it has six neighbours" do
        it "should die by overcrowding" do
          cell = GameOfLifeCell.new(1,2,true,1)
          expect(cell.dead)to eq true
        end
      end

      context "and it has seben neighbours" do
        it "should die by overcrowding" do
          cell = GameOfLifeCell.new(1,2,true,1)
          expect(cell.dead)to eq true
        end
      end

     context "and it has eight neighbours" do
        it "should die by overcrowding" do
          cell = GameOfLifeCell.new(1,2,true,1)
          expect(cell.dead)to eq true
        end
      end

    end

    # 4 rule: a dead cell becomes a live cell if it has just 3 neighbours
    context "When the given cell is dead" do
      context "and it has just three neighbours" do
        it "should becomes a live cell by reproduction"
          cell = GameOfLifeCell.new(1,2,false,1)
          expect(cell.dead).to eq false
        end
      end
    end
