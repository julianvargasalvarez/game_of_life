require 'test/unit'

Class TestGameOfLife < Test::Unit::TestCase
  def test_si_la_celda_vive_retorna_true
    assert_equals true, celda_viva(celda) 
  end
 
  def test_si_la_celda_muere_retorna_false
    assert_equals false, celda_viva(celda)  
  end

  def test_celda_muere_por_poca_poblacion_regla_1
    assert_equals true, pocaPoblacionRegla1(celda)    
  end

  def test_celda_vive_siguiente_generacion_regla_2
    
  end

  def test_celda_muere_por_sobrepoblacion_regla_3
  end

  def test_celda_revive_por_reproduccion_regla_4
  end
end
