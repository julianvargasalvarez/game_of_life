require 'rspec'

class GameOfLifeCell
  attr_accessor :dead
  attr_accessor :c_x
  attr_accessor :c_y
  attr_accessor :neigbours
 
  def initialize(x, y, status, n)
    self.dead = status
    self.c_x = x
    self.c_y = y
    self.neigbours = n 
  end
end

describe "Game of life rules" do
  describe "steps for changing the state for a given cell" do
    context "when the given cell is alive" do
      context "and it has only one neighbor" do
        it "should died for underpopulation" do
          cell = GameOfLifeCell.new(1, 2, true, 1)
          expect(cell.dead).to eq true
        end
      end

      context "and it has two neighbours" do
        it "should live on to the next generation" do
          cell = GameOfLifeCell.new(1, 2, false, 2)
          expect(cell.dead).to eq false
        end
      end

      context "and it has three neighbours" do
        it "should live on to the next generation" do
          cell = GameOfLifeCell.new(1, 2, false, 3)
          expect(cell.dead).to eq false
        end
      end

      context "and it has four neighbours" do
        it "should died for high population" do
          cell = GameOfLifeCell.new(1, 2, true, 4)
          expect(cell.dead).to eq true
        end
      end

      context "and it has five neighbours" do
        it "should died for high population" do
          cell = GameOfLifeCell.new(1, 2, true, 5)
          expect(cell.dead).to eq true
        end
      end
      
      context "and it has six neighbours" do
        it "should died for high population" do
          cell = GameOfLifeCell.new(1, 2, true, 6)
          expect(cell.dead).to eq true
        end
      end

      context "and it has seven neighbours" do
        it "should died for high population" do
          cell = GameOfLifeCell.new(1, 2, true, 7)
          expect(cell.dead).to eq true
        end
      end

      context "and it has eight neighbours" do
        it "should died for high population" do
          cell = GameOfLifeCell.new(1, 2, true, 8)
          expect(cell.dead).to eq true
        end
      end
    end
    context "when the given cell is dead" do
      context "and it has three live neighbours" do
        it "should live for reproduction" do
          cell = GameOfLifeCell.new(1, 2, false, 3)
          expect(cell.dead).to eq false
        end
      end
    end
  end
end
