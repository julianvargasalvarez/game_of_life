Feature: game of life its a cycle of life of cells that alive,dead
As a machine 
I want steps for changing the state for a given cell 
So that genera un autómata celular

Scenario 1: its live with a neighbour
Given when cell alive
When has only one neighbours
Then ensure of status is dead  for underpopulation, his status is dead

Scenario 2: its  live with two neighbours
Given when cell alive
When has only two neighbours
Then ensure a live in the next generation

Scenario 3: its live with three neighbours
Given when cell alive
When has only three neighbours
Then ensure a live in the next generation

Scenario 4: its live with four neighbours
Given when cell alive  
When has only four neighbours
Then ensure of status is dead   for high population

Scenario 5: its live with five neighbours
Given when cell alive  
When has only five neighbours
Then ensure of status is dead   for high population

Scenario 6: its live with six neighbours
Given when cell alive  
When has only six neighbours
Then ensure of status is dead   for high population

Scenario 7: its live with seven neighbours
Given when cell alive  
When has only seven neighbours
Then ensure of status is dead   for high population

Scenario 8: its live with eight neighbours
Given when cell alive  
When has only eight neighbours
Then ensure of status is dead   for high population

Scenario 9: its dead  with three neighbours
Given when cell is dead
When has only three neighbours
Then should live for reproduction
