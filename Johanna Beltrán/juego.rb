require 'test/unit'
def celula_muere(vecinos)
  return false unless vecinos < 2
  true
end

def celula_siguiente_generacion(vecinos)
  return false unless vecinos == 2 || vecinos == 3
  true
end

def celula_muere(vecinos)
  return false unless vecinos > 3
  true
end

def celula_muerta_sobrevive(vecinos)
  return false unless vecinos == 3
  true
end

class TestJuegoGameOfLife < Test::Unit::TestCase
  def test_celula_viva_con_menos_de_dos_vecinos_vivos_muere
    assert_equal true, celula_muere(1)
  end

  def test_celula_viva_con_dos_o_tres_vecinos_vivos_viven_siguiente_generacion
    assert_equal true, celula_siguiente_generacion(2)
    assert_equal true, celula_siguiente_generacion(3)
  end

  def test_celula_viva_con_mas_de_tres_vecinos_vivos_muere
    assert_equal true, celula_muere(4)
    assert_equal true, celula_muere(5)
    assert_equal true, celula_muere(6)
  end

  def test_celula_muerta_con_tres_vecinos_vivos_se_convierte_en_celula_viva
    assert_equah true, celula_muerta_sobrevive(3)
  end
end
