require 'rspec'
 
 class GameOfLifeCell
   attr_accessor :alive
   attr_accessor :x
   attr_accessor :y
   attr_accessor :neighbors
 
   def initialize(x, y, status, n)
     self.alive = status
     self.x = x
     self.y = y
     self.neighbors = n
   end
 end
 
 describe "Game of life rules" do
   describe "steps for changing the state for a given cell" do
         it "should died for underpopulation" do
           cell = GameOfLifeCell.new(1, 2, true, 1)
           expect(cell.alive).to eq true
         end
       end
       
       context "and it has two neighbors" do
         it "should live to the next generation" do
           cell = GameOfLifeCell.new(1, 2, false, 2)
           expect(cell.alive).to eq false
         end
       end
 
       context "and it has three neighbors" do 
         it "should live on to the next generation" do
           cell = GameOfLifeCell.new(1, 2, false, 3)
           expect(cell.alive).to eq false
         end
       end
 
       context "and it has four neighbors" do
         it "should dead as if by over-population" do
           cell = GameOfLifeCell.new(1, 2, true, 4)
           expect(cell.alive).to eq true
         end
       end
 
       context "and it has five neighbors" do
         it "should dead as if by over-population" do
           cell = GameOfLifeCell.new(1, 2, true, 5)
           expect(cell.alive).to eq true
         end
       end
 
       context "and it has six neighbors" do
         it "should dead as if by over-population" do
           cell = GameOfLifeCell.new(1, 2, true, 6)
           expect(cell.alive).to eq true
         end
       end
 
       context "and it has seven neighbors" do
         it "should dead as if by over-population" do
           cell = GameOfLifeCell.new(1, 2, true, 7)
           expect(cell.alive).to eq true
         end
       end
 
       context "and it has eigth neighbors" do
         it "should dead as if by over-population" do
           cell = GameOfLifeCell.new(1, 2, true, 8)
           expect(cell.alive).to eq true
         end
       end
     end
  
     context "when the give cell is dead" do
       context "and it has three neighbors" do
         it "should live as if by reproduction" do
           cell = GameOfLifeCell.new(1, 2, false, 3)
           expect(cell.alive).to eq false
         end
       end
     end
   end
