require 'test/unit'

def celulaMuertaNace(cantidadVecinas)
  return false
end

def sigueViva(cantidadVecinas)
  return false
end

class TestReglasGameOfLifeCelulaMuerta < Test::Unit::TestCase
  def test_si_celula_muerta_con_3_vecinas_deberia_nacer
     assert_equal true, celulaMuertaNace(3)
  end

  def test_si_celula_muerta_con_1_vecina_deberia_seguir_muerta
     assert_equal false, celulaMuertaNace(1)
  end
end

class TestReglasGameOfLifeCelulaViva < Test::Unit::TestCase
  def test_si_celula_viva_2_o_3_vecinas_sigue_viva
     assert_equal true, sigueViva(2)
  end

  def tets_si_celula_viva_2_o_3_vecinas_sigue_viva
     assert_equal true, sigueViva(3)
  end

  def test_si_celula_viva_2_o_3_vecinas_sigue_viva
     assert_equal false, sigueViva(1)
  end

  def test_si_celula_viva_2_o_3_vecinas_sigue_viva
     assert_equal false, sigueViva(4)
  end

end
