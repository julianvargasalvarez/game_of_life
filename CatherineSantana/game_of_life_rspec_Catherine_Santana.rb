require 'rspec'

class GameOfLifeCell
  attr_accessor :dead

  def initialize(x, y, status, n)
    self.dead = status
  end
end

describe "Game of life rules" do
  describe "steps for changing the state for a given cell" do
    context "when the given cell is alive" do
      context "and it has only one neighbor" do
        it "should died for underpopulation" do
          cell = GameOfLifeCell.new(1, 2, true, 1)
          expect(cell.dead).to eq true
        end
      end
      
      context "and it has two neighbors" do
        it "should live to the next generation" do
          cell = GameOfLifeCell.new(1, 2, false, 2)
          expect(cell.dead).to eq false
        end
      end

      context "and it has three neighbors" do 
        it "should live on to the next generation" do
          cell = GameOfLifeCell.new(1, 2, false, 3)
          expect(cell.dead).to eq false
        end
      end

      context "and it has four neighbors" do
        it "should dead for overcrowding" do
          cell = GameOfLifeCell.new(1, 2, true, 4)
          expect(cell.dead).to eq true
        end
      end

      context "and it has five neighbors" do
        it "should dead as if by overcrowding" do
          cell = GameOfLifeCell.new(1, 2, true, 5)
          expect(cell.dead).to eq true
        end
      end

      context "and it has six neighbors" do
        it "should dead as if by overcrowding" do
          cell = GameOfLifeCell.new(1, 2, true, 6)
          expect(cell.dead).to eq true
        end
      end

      context "and it has seven neighbors" do
        it "should dead as if by overcrowding" do
          cell = GameOfLifeCell.new(1, 2, true, 7)
          expect(cell.dead).to eq true
        end
      end

      context "and it has eigth neighbors" do
        it "should dead as if by overcrowding" do
          cell = GameOfLifeCell.new(1, 2, true, 8)
          expect(cell.dead).to eq true
        end
      end
    end
 
    context "when the give cell is dead" do
      context "and it has three neighbors" do
        it "should live as if by reproduction" do
          cell = GameOfLifeCell.new(1, 2, false, 3)
          expect(cell.dead).to eq false
        end
      end
    end
  end
end
