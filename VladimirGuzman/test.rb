require 'test/unit'

def live_or_die()
end 

class TestGameOfLife < Test::Unit::TestCase
  
  def test_any_live_cell_with_fewer_than_two_live_neighbours_dies
    assert_equal true, false
  end

  def test_any_live_cell_with_two_or_three_live_neighbours_lives_on_to_the_next_generation
    assert_equal true, false
  end

  def test_any_live_cell_with_more_than_three_live_neighbours_dies_as_if_by_overcrowding
    assert_equal true, false
  end

  def test_any_dead_cell_with_exactly_three_live_neighbours_becomes_a_live_cell_as_if_by_reproduction
    assert_equal true, false
  end


end
