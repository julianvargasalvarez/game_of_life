require 'rspec'

class GameOfLifeMatriz
end

class GameOfLifeCell
  attr_accessor :live
  def initialize(x, y, status, n)
    self.live = status
  end
end

describe "Game of life rules" do
  describe "steps for changing the state for a given cell" do
    context "when the given cell is alive" do
      context "and it has only one neighbor" do
        it "should died for underpopulation" do
          cell = GameOfLifeCell.new(1, 2, true, 1)
          expect(cell.live).to eq false
        end
      end
      context "and it has two or three live neighbours" do
        context "with two live neighbours" do
          it "should live on the next generation" do 
            cell = GameOfLifeCell.new(1, 2, true, 2)
            expect(cell.live).to eq true
          end
        end
        context "with three live neighbours" do
          it "should live on the next generation" do
            cell = GameOfLifeCell.new(1, 2, true, 3)
            expect(cell.live).to eq true
          end
        end
      end 
      context "and it has more than three live neighbours" do
        context "with four neighbours" do
          it "should died for overcrowding" do
            cell = GameOfLifeCell.new(1, 2, true, 4)
            expect(cell.live).to eq false
          end
        end
        context "with five neighbours" do
          it "should died for overcrowding" do
            cell = GameOfLifeCell.new(1, 2, true, 5)
            expect(cell.live).to eq false
          end
        end
        context "with six neighbours" do
          it "should died for overcrowding" do
            cell = GameOfLifeCell.new(1, 2, true, 6)
            expect(cell.live).to eq false
          end
        end
        context "with seven neighbours" do
          it "should died for overcrowding" do
            cell = GameOfLifeCell.new(1, 2, true, 7)
            expect(cell.live).to eq false
          end
        end
        context "with eight neighbours" do
          it "should died for overcrowding" do
            cell = GameOfLifeCell.new(1, 2, true, 8)
            expect(cell.live).to eq false
          end
        end
      end
    end
    context "when the given cell is dead" do
      context "and it has exactly three live neighbours" do
        it "should becomes a live by reproduction" do
          cell = GameOfLifeCell.new(1, 2, false, 3)
          expect(cell.live).to eq true
        end
      end
    end
  end
end
